import java.util.ArrayList;
import java.util.Scanner;

/*
 * Allows the user to play Video poker. Class manages all I/O
 */
public class PokerSimulator {
	static Hand hand;
	private static ArrayList<Card> cardDisplayed;
	static Deck deck;
	final static int ROYAL_FLUSH = 250;
	final static int STRAIGHT_FLUSH = 50;
	final static int FOUR_OF_A_KIND = 25;
	final static int FULL_HOUSE = 6;
	final static int FLUSH = 5;
	final static int STRAIGHT = 4;
	final static int THREE_OF_A_KIND = 3;
	final static int TWO_PAIR = 2;
	final static int ONE_PAIR = 1;
	static int javaDollars = 100;
	static int amountSpent = 0;

	/*
	 * Main method of the class. Creates the deck, sets up the game and displays results to player.
	 */
	public static void main(String[] args) {
		
		deck = new Deck();
		deck.shuffle();
		System.out.println("Your initial amount is 100 JavaDollars");
		Scanner in = new Scanner(System.in);
		boolean keepPlaying = true;
		while (keepPlaying) {
			createHand();
			scoreHand();
			System.out
					.println("You have: "
							+ (javaDollars - amountSpent)
							+ " JavaDollars remaining.");
			addHandToRejectPile();
			System.out.println("Enter anything to continue playing. Type \"n\" to stop");
			if (in.next().equals("n")) {
				keepPlaying = false;
			}
		}

	}

	/*
	 * Adds the hand to the reject pile after hand is scored.
	 */
	private static void addHandToRejectPile() {
		for (int i = 0; i < 5; i++) {
			deck.rejectedDeck(cardDisplayed.get(i));
			cardDisplayed.set(i, deck.deal());
		}
		for (Card card : cardDisplayed) {
			deck.rejectedDeck(card);
		}
	}

	/*
	 * Scores the hand and determines result and the JavaDollars user gets.
	 */
	private static void scoreHand() {

		if (hand.isRoyalFlush()) {
			System.out.println("You Scored a Royal Flush! You win "
					+ ROYAL_FLUSH + " JavaDollars!");
			javaDollars += ROYAL_FLUSH;
		} else if (hand.isStraightFlush()) {
			System.out.println("You Scored a Straight Flush! You win "
					+ STRAIGHT_FLUSH + " JavaDollars!");
			javaDollars += STRAIGHT_FLUSH;
		} else if (hand.isFourOfaKind()) {
			System.out.println("You Scored a Four of a Kind! You win "
					+ FOUR_OF_A_KIND + " JavaDollars!");
			javaDollars += FOUR_OF_A_KIND;
		} else if (hand.isFullHouse()) {
			System.out.println("You Scored a Full House! You win " + FULL_HOUSE
					+ " JavaDollars!");
			javaDollars += FULL_HOUSE;
		} else if (hand.isFlush()) {
			System.out.println("You Scored a Flush! You win " + FLUSH
					+ " JavaDollars!");
			javaDollars += FLUSH;
		} else if (hand.isStraight()) {
			System.out.println("You Scored a Straight! You win " + STRAIGHT
					+ " JavaDollars!");
			javaDollars += STRAIGHT;
		} else if (hand.isThreeOfaKind()) {
			System.out.println("You Scored a Three of a Kind! You win "
					+ THREE_OF_A_KIND + " JavaDollars!");
			javaDollars += THREE_OF_A_KIND;
		} else if (hand.isTwoPairs()) {
			System.out.println("You Scored a Two Pair! You win " + TWO_PAIR
					+ " JavaDollars!");
			javaDollars += TWO_PAIR;
		} else if (hand.isOnePair()) {
			System.out
					.println("You Scored a Pair of Jacks or Better (One Pair)! You win "
							+ ONE_PAIR + " JavaDollars!");
			javaDollars += ONE_PAIR;
		} else {
			System.out.println("You Scored no pairs =(. ");
		}
		amountSpent++;
	}

	/*
	 * Creates the hand. Shows cards to user and gets output based on user input.
	 */
	private static void createHand() {

		if (deck.getDeckSize() <= 10) {
			deck = new Deck();
			deck.shuffle();
		}

		Scanner in = new Scanner(System.in);

		cardDisplayed = new ArrayList<Card>();

		for (int i = 0; i < 5; i++) {
			cardDisplayed.add(deck.deal());
		}

		System.out.println("Your Cards [" + cardDisplayed.get(0) + ", "
				+ cardDisplayed.get(1) + ", " + cardDisplayed.get(2) + ", "
				+ cardDisplayed.get(3) + ", " + cardDisplayed.get(4) + "]");
		System.out
				.println("To reject cards enter : \"n\" for none, \"s\" for some and \"a\" for all");
		String rejectInput = in.next();

		if (rejectInput.equals("n")) {
			hand = new Hand();
			for (Card card : cardDisplayed) {
				hand.add(card);
			}

			System.out.println("Your hand is: " + hand);
		} else if (rejectInput.equals("s")) {
			hand = new Hand();
			System.out.println("Enter the number of cards you want to reject");

			int numOfRejections = in.nextInt();
			System.out
					.println("Enter the index of the cards that you want to reject. Index starts at 1 and ends at 5. Seperate the index with a space.");
			for (int i = 0; i < numOfRejections; i++) {
				int r = in.nextInt() - 1;
				deck.rejectedDeck(cardDisplayed.get(r));
				cardDisplayed.set(r, deck.deal());
			}
			for (Card card : cardDisplayed) {
				hand.add(card);
			}

			System.out.println("Your hand is: " + hand);
		} else if (rejectInput.equals("a")) {
			hand = new Hand();
			for (int i = 0; i < 5; i++) {
				deck.rejectedDeck(cardDisplayed.get(i));
				cardDisplayed.set(i, deck.deal());
			}
			for (Card card : cardDisplayed) {
				hand.add(card);
			}
			System.out.println("Your hand: " + hand);
		}
	}
}
