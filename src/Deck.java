import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents a deck of 52 cards.
 */
public class Deck {
	private ArrayList<Card> cards;
	private ArrayList<Card>	rejectedCards;
	/**
	 * Constructs the deck
	 */
	public Deck() {
		cards = new ArrayList<Card>();
		rejectedCards = new ArrayList<Card>();
		cards.add(new Card(Card.Face.Ace, Card.Suit.Spades));
		cards.add(new Card(Card.Face.two, Card.Suit.Spades));
		cards.add(new Card(Card.Face.three, Card.Suit.Spades));
		cards.add(new Card(Card.Face.four, Card.Suit.Spades));
		cards.add(new Card(Card.Face.five, Card.Suit.Spades));
		cards.add(new Card(Card.Face.six, Card.Suit.Spades));
		cards.add(new Card(Card.Face.seven, Card.Suit.Spades));
		cards.add(new Card(Card.Face.eight, Card.Suit.Spades));
		cards.add(new Card(Card.Face.nine, Card.Suit.Spades));
		cards.add(new Card(Card.Face.ten, Card.Suit.Spades));
		cards.add(new Card(Card.Face.Jack, Card.Suit.Spades));
		cards.add(new Card(Card.Face.Queen, Card.Suit.Spades));
		cards.add(new Card(Card.Face.King, Card.Suit.Spades));
		cards.add(new Card(Card.Face.Ace, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.two, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.three, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.four, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.five, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.six, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.seven, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.eight, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.nine, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.ten, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.Jack, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.Queen, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.King, Card.Suit.Diamonds));
		cards.add(new Card(Card.Face.Ace, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.two, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.three, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.four, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.five, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.six, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.seven, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.eight, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.nine, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.ten, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.Jack, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.Queen, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.King, Card.Suit.Clubs));
		cards.add(new Card(Card.Face.Ace, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.two, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.three, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.four, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.five, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.six, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.seven, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.eight, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.nine, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.ten, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.Jack, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.Queen, Card.Suit.Hearts));
		cards.add(new Card(Card.Face.King, Card.Suit.Hearts));
	}

	/**
	 * Returns a string representation of the deck
	 * 
	 * @return a string representation of the deck
	 */
	public String toString() {
		return cards.toString();
	}

	/**
	 * Deals the top card from the deck and removes from the deck.
	 * 
	 * @return the top card from the deck Precondition: the deck is not empty.
	 */
	public Card deal() {
		Card card = cards.remove(cards.size() - 1);
		return card;
	}

	/**
	 * Checks if the deck is empty
	 * 
	 * @return true if the deck is empty and false otherwise.
	 */
	public boolean isEmpty() {
		return cards.size() == 0;
	}

	/**
	 * Shuffles the deck
	 */
	public void shuffle() {
	    Collections.shuffle(cards);
	}
	
	/*
	 * Gets the size of the deck.
	 * @return size of deck
	 */
	public int getDeckSize(){
		return cards.size();
	}
	
	/*Adds rejected cards to rejected pile
	 * @param card the card to add to the rejected pile
	 */
	public void rejectedDeck(Card card){
		rejectedCards.add(card);
	}
}