/**
   Tests a Card. Tester Class
 */
import java.util.*;

public class PokerTester {
	public static void main(String[] args) {
		Card card = new Card(Card.Face.Ace, Card.Suit.Hearts);
		System.out.println(card);
		card = new Card(Card.Face.King, Card.Suit.Spades);
		System.out.println(card);
		Deck deck = new Deck();
		System.out.println("deck = " + deck.toString());
		System.out.println("Expected: [ the full deck in order ]");
		deck.shuffle();
		System.out.println("deck = " + deck.toString());
		System.out.println("Expected: [ shuffeled deck ]");
		System.out.println("Dealing top card: " + deck.deal());
		System.out.println("Dealing top card: " + deck.deal());
		System.out.println("Dealing top card: " + deck.deal());
		System.out.println("Dealing top card: " + deck.deal());
		System.out.println("Expected: [ cards from the top of the deck]");
	
		Hand hand = new Hand();
		hand.add(card);

		System.out.println("hand = " + hand);
		card = deck.deal();
		hand.add(card);
		System.out.println("hand = " + hand);
		hand.add(deck.deal());
		hand.add(deck.deal());
		hand.add(deck.deal());
		
		System.out.println("hand = " + hand);
	
		System.out.println("______________________________________");
		hand = new Hand();
		card = new Card(Card.Face.King, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.Ace, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.Jack, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.Queen, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.ten, Card.Suit.Spades);
		hand.add(card);
		System.out.println("hand = " + hand);
		System.out.println("is it Royal Flush? " + hand.isRoyalFlush());
		System.out.println("Expected: true");
		System.out.println("is it Straight? " + hand.isStraight());
		System.out.println("Expected: true");
		System.out.println("is it Flush? " + hand.isFlush());
		System.out.println("Expected: true");
		hand.sortHandByFace();
		System.out.println("hand = " + hand);
		System.out.println("Expected: Hand  sorted in proper rank");
		System.out.println("is it Straight Flush? " + hand.isStraightFlush());
		System.out.println("Expected: true");
		System.out.println("is it Three of a Kind? " + hand.isThreeOfaKind());
		System.out.println("Expected: false");	
		System.out.println("______________________________________");
	
		
		hand = new Hand();
		card = new Card(Card.Face.King, Card.Suit.Diamonds);
		hand.add(card);
		card = new Card(Card.Face.Ace, Card.Suit.Hearts);
		hand.add(card);
		card = new Card(Card.Face.five, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.Queen, Card.Suit.Clubs);
		hand.add(card);
		card = new Card(Card.Face.ten, Card.Suit.Spades);
		hand.add(card);
		System.out.println("hand = " + hand);
		System.out.println("is it Royal Flush? " + hand.isRoyalFlush());
		System.out.println("Expected: false");
		System.out.println("is it Flush? " + hand.isFlush());
		System.out.println("Expected: false");
		System.out.println("is it Four of a Kind? " + hand.isFourOfaKind());
		hand.sortHandByFace();
		System.out.println("hand = " + hand);
		System.out.println("Expected: Hand  sorted in proper rank");
		System.out.println("is it Straight? " + hand.isStraight());
		System.out.println("Expected: false");
		System.out.println("is it One Pairs? " + hand.isOnePair());
		System.out.println("Expected: false");
		System.out.println("______________________________________");
		
		hand = new Hand();
		card = new Card(Card.Face.Ace, Card.Suit.Diamonds);
		hand.add(card);
		card = new Card(Card.Face.Ace, Card.Suit.Hearts);
		hand.add(card);
		card = new Card(Card.Face.Ace, Card.Suit.Clubs);
		hand.add(card);
		card = new Card(Card.Face.Ace, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.ten, Card.Suit.Diamonds);
		hand.add(card);
		System.out.println("hand = " + hand);
		System.out.println("is it Four of a Kind? " + hand.isFourOfaKind());
		System.out.println("Expected: true");
		System.out.println("is it Straight? " + hand.isStraight());
		System.out.println("Expected: false");
		hand.sortHandByFace();
		System.out.println("hand = " + hand);
		System.out.println("Expected: Hand  sorted in proper rank");
		System.out.println("is it Straight Flush? " + hand.isStraightFlush());
		System.out.println("Expected: false");
		System.out.println("is it Three of a Kind? " + hand.isThreeOfaKind());
		System.out.println("Expected: true");	
		System.out.println("is it Two Pairs? " + hand.isTwoPairs());
		System.out.println("Expected: true");
		System.out.println("is it One Pairs? " + hand.isOnePair());
		System.out.println("Expected: true");
		System.out.println("______________________________________");
		
		hand = new Hand();
		card = new Card(Card.Face.five, Card.Suit.Diamonds);
		hand.add(card);
		card = new Card(Card.Face.six, Card.Suit.Hearts);
		hand.add(card);
		card = new Card(Card.Face.seven, Card.Suit.Clubs);
		hand.add(card);
		card = new Card(Card.Face.eight, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.nine, Card.Suit.Diamonds);
		hand.add(card);
		System.out.println("hand = " + hand);
		System.out.println("is it Straight? " + hand.isStraight());
		System.out.println("Expected: true");
		System.out.println("is it Straight Flush? " + hand.isStraightFlush());
		System.out.println("Expected: false");
		System.out.println("is it Full House? " + hand.isFullHouse());
		System.out.println("Expected: false");
		System.out.println("is it Three of a Kind? " + hand.isThreeOfaKind());
		System.out.println("Expected: false");	
		System.out.println("is it Two Pairs? " + hand.isTwoPairs());
		System.out.println("Expected: false");
		System.out.println("is it One Pairs? " + hand.isOnePair());
		System.out.println("Expected: false");
		System.out.println("______________________________________");
		
		hand = new Hand();
		card = new Card(Card.Face.five, Card.Suit.Diamonds);
		hand.add(card);
		card = new Card(Card.Face.five, Card.Suit.Hearts);
		hand.add(card);
		card = new Card(Card.Face.five, Card.Suit.Clubs);
		hand.add(card);
		card = new Card(Card.Face.nine, Card.Suit.Spades);
		hand.add(card);
		card = new Card(Card.Face.nine, Card.Suit.Diamonds);
		hand.add(card);
		System.out.println("hand = " + hand);
		System.out.println("is it Full House? " + hand.isFullHouse());
		System.out.println("Expected: true");
		System.out.println("is it Three of a Kind? " + hand.isThreeOfaKind());
		System.out.println("Expected: true");	
		System.out.println("is it Two Pairs? " + hand.isTwoPairs());
		System.out.println("Expected: true");
		System.out.println("is it One Pairs? " + hand.isOnePair());
		System.out.println("Expected: false");
		System.out.println("______________________________________");
	
		
	}
}