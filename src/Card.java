/**
 * This class represents a playing card. It has a suit and a value.
 */
public class Card implements Comparable<Card> {
	public enum Suit {
		Spades, Hearts, Diamonds, Clubs
	}

	public enum Face {
		two, three, four, five, six, seven, eight, nine, ten, Jack, Queen, King, Ace
	}

	private Suit suit;
	private Face face;
	private int rank;

	/**
	 * Constructs a card given a suit and face
	 * 
	 * @param suit
	 *            the suit of the card
	 * @param face
	 *            the face of the card
	 */
	public Card(Face face, Suit suit) {
		this.suit = suit;
		this.face = face;
		rank = this.face.ordinal();
	}

	/**
	 * Returns a string for this card.
	 * 
	 * @return a string representing the card
	 */
	public String toString() {
		return "" + face + " of " + suit;
	}

	/*
	 * Gets the suit of the cards
	 * @return the suit of the card
	 */
	public Suit getSuit() {
		return suit;
	}
	
	/*
	 * Gets the face of the card
	 * @return face of the card
	 */
	public Face getFace() {

		return face;
	}

	/*
	 * Gets the rank of the card.
	 * 
	 * @return rank of the card
	 */
	public int getFaceRank() {
		return rank;
	}

	/*
	 * (non-Javadoc) Overrides compareTo method from Collections class. Allows
	 * for cards to be sorted
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Card arg0) {
		int compareQuantity = ((Card) arg0).getFaceRank();
		// ascending order
		return this.rank - compareQuantity;

		// descending order
		// return compareQuantity - this.rank;
	}
}