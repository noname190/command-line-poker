import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents a hand of cards
 */
public class Hand {
	private ArrayList<Card> cards;

	/**
	 * Constructs an empty hand
	 */
	public Hand() {
		cards = new ArrayList<Card>();
	}

	/**
	 * Adds a card to the hand
	 * 
	 * @param card
	 *            is the card to add to the hand
	 * 
	 */
	public void add(Card card) {
		cards.add(card);
	}

	/**
	 * Returns a string representation of the hand.
	 * 
	 * @return a string representation of the hand.
	 */
	public String toString() {
		return cards.toString();
	}

	/*
	 * Determines if deck is Royal Flush
	 * @return true if deck is Royal Flush 
	 */
	public boolean isRoyalFlush() {

		Card card = cards.get(0);
		Enum suit = card.getSuit();
		// Enum face = card.getFace();
		// if hand is not all same suit return false
		if (cards.get(1).getSuit() == suit && cards.get(2).getSuit() == suit
				&& cards.get(3).getSuit() == suit
				&& cards.get(4).getSuit() == suit) {
			// if hand doesn't contain right faces return false
			for (int i = 0; i < cards.size(); i++) {
				if (!(cards.get(i).getFace() == Card.Face.Ace
						|| cards.get(i).getFace() == Card.Face.King
						|| cards.get(i).getFace() == Card.Face.Queen
						|| cards.get(i).getFace() == Card.Face.Jack || cards
						.get(i).getFace() == Card.Face.ten)) {
					return false;
				}

			}
			return true;
		}
		return false;

	}

	/*
	 * Determines if deck is Three of a Kind
	 * @return true if deck is Three of a Kind 
	 */
	public boolean isThreeOfaKind() {
		sortHandByFace();
		if (cards.get(0).getFaceRank() == cards.get(1).getFaceRank()
				&& cards.get(1).getFaceRank() == cards.get(2).getFaceRank()) {
			return true;
		} else if (cards.get(1).getFaceRank() == cards.get(2).getFaceRank()
				&& cards.get(2).getFaceRank() == cards.get(3).getFaceRank()) {
			return true;
		} else if (cards.get(2).getFaceRank() == cards.get(3).getFaceRank()
				&& cards.get(3).getFaceRank() == cards.get(4).getFaceRank()) {
			return true;
		}
		return false;
	}

	/*
	 * Determines if deck is Flush
	 * @return true if deck is Flush 
	 */
	public boolean isFlush() {

		Enum suit = cards.get(0).getSuit();
		if (cards.get(1).getSuit() == suit && cards.get(2).getSuit() == suit
				&& cards.get(3).getSuit() == suit
				&& cards.get(4).getSuit() == suit) {
			return true;
		}
		return false;
	}

	/*
	 * Determines if deck is Four of a Kind
	 * @return true if deck is Four of a Kind 
	 */
	public boolean isFourOfaKind() {
		for (int i = 0; i < cards.size(); i++) {
			int same = 1;
			Enum suit = cards.get(i).getFace();
			int r = 0;

			while (r < cards.size()) {
				if (r == i && r != cards.size() - 1) {
					r++;
				}
				if (suit == cards.get(r).getFace()) {
					same++;
				}

				if (same >= cards.size() - 1) {
					return true;
				}
				r++;
			}
		}
		return false;
	}

	/*
	 * Determines if deck is Full House
	 * @return true if deck is Full House 
	 */
	public boolean isFullHouse() {
		// if 0 = 1 = 2 && (3 = 4) have same rank
		// or if (0 = 1) && 2 = 3 = 4
		if ((cards.get(0).getFaceRank() == cards.get(1).getFaceRank()
				&& cards.get(1).getFaceRank() == cards.get(2).getFaceRank() && cards
				.get(3).getFaceRank() == cards.get(4).getFaceRank())
				|| (cards.get(0).getFaceRank() == cards.get(1).getFaceRank()
						&& cards.get(2).getFaceRank() == cards.get(3)
								.getFaceRank() && cards.get(3).getFaceRank() == cards
						.get(4).getFaceRank())) {
			return true;
		}

		return false;
	}

	/*
	 * Determines if deck is Straight Flush
	 * @return true if deck is Straight Flush 
	 */
	public boolean isStraightFlush() {
		if (isStraight() && isFlush()) {
			return true;
		}
		return false;
	}

	/*
	 * Determines if deck is Straight
	 * @return true if deck is Straight
	 */
	public boolean isStraight() {
		/*
		 * int min = 0; int minRank = 0; int maxRank = 0; int max = 0; for(int i
		 * = 0; i < cards.size(); i++){ min = cards.get(i).getFace().ordinal();
		 * max = cards.get(i).getFace().ordinal();
		 * if(cards.get(i).getFace().ordinal() <= min){ min = i; minRank =
		 * cards.get(i).getFace().ordinal() ; }
		 * if(cards.get(i).getFace().ordinal() >= max){ max = i; maxRank =
		 * cards.get(i).getFace().ordinal() ; } }
		 */
		sortHandByFace();
		if (cards.get(4).getFaceRank() == 12) {
			if (cards.get(3).getFace() == Card.Face.King
					&& cards.get(2).getFace() == Card.Face.Queen
					&& cards.get(1).getFace() == Card.Face.Jack
					&& cards.get(0).getFace() == Card.Face.ten) {
				return true;
			} else if (cards.get(3).getFace() == Card.Face.five
					&& cards.get(2).getFace() == Card.Face.four
					&& cards.get(1).getFace() == Card.Face.three
					&& cards.get(0).getFace() == Card.Face.two) {
				return true;
			}
		} else {
			int rank = cards.get(0).getFaceRank();
			for (int i = 1; i < cards.size(); i++) {
				if (cards.get(i).getFaceRank() != rank + 1) {
					return false;
				}
				rank++;
			}
			return true;
		}

		return false;

	}

	/*
	 * Determines if deck is Two Pair
	 * @return true if deck is Two Pair
	 */
	public boolean isTwoPairs(){
		sortHandByFace();
		if (cards.get(0).getFaceRank() == cards.get(1).getFaceRank()
				&& cards.get(2).getFaceRank() == cards.get(3).getFaceRank()) {
			return true;
		} else if (cards.get(0).getFaceRank() == cards.get(1).getFaceRank()
				&& cards.get(3).getFaceRank() == cards.get(4).getFaceRank()) {
			return true;
		} else if (cards.get(1).getFaceRank() == cards.get(2).getFaceRank()
				&& cards.get(3).getFaceRank() == cards.get(4).getFaceRank()) {
			return true;
		}
		return false;
		
		
	}
	
	/*
	 * Determines if deck is One Pair
	 * @return true if deck is One Pair
	 */
	public boolean isOnePair(){
		
		sortHandByFace();
		if (cards.get(0).getFaceRank() == cards.get(1).getFaceRank() && cards.get(1).getFaceRank() >= Card.Face.Jack.ordinal()) {
			return true;
		} else if (cards.get(1).getFaceRank() == cards.get(2).getFaceRank() && cards.get(2).getFaceRank() >= Card.Face.Jack.ordinal()) {
			return true;
		} else if (cards.get(2).getFaceRank() == cards.get(3).getFaceRank() && cards.get(3).getFaceRank() >= Card.Face.Jack.ordinal() ) {
			return true;
		}else if (cards.get(3).getFaceRank() == cards.get(4).getFaceRank() && cards.get(4).getFaceRank() >= Card.Face.Jack.ordinal()) {
			return true;
		}
		return false;
	}
	
	/*
	 * Shuffles the deck
	 * 
	 */
	public void sortHandByFace() {
		Collections.sort(cards);
	}

}